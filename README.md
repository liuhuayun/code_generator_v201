## 介绍
代码生成器，可一键生成controller，service，dao，实体类，单表、多表的sql语句，日志处理、事务支持等，同时可以生成dubbo和springCloud脚手架方便开发微服务项目，能在很大程度上提高开发效率，节约开发时间。代码生成完毕后即为一个前台到后台的完整项目，导入eclipse/idea便可运行，可根据需求自由扩展！
## 使用方法
idea插件使用方式：详情见[https://gitee.com/zrxjava/codeMan.git](https://gitee.com/zrxjava/codeMan.git)，强烈建议使用idea插件形式安装使用！<br/>
应用程序使用方式：下载后解压，windows下双击exe程序即可运行，mac下双击command程序即可运行，更新方式为在线更新，无需反复下载，下载一次，永久使用，此为代码生成器客户端，作用是从服务器获取生成器运行所需要的文件，生成器主体源码已开放：[https://gitee.com/zrxjava/codeMan.git](https://gitee.com/zrxjava/codeMan.git)